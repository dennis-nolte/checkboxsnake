package javafx.checkboxsnake.game;

/**
 * javaDoc
 *
 * @author Dennis Nolte
 */
public class GameSettings {
    public static int GAME_FIELD_SIZE = 15;
    public static long GAME_SPEED = 150;

    public static boolean SPECIAL_FOOD_ENABLED = false;
    public static final int GAME_LOOPS_TILL_SPECIAL_FOOD = 30;

    public static double SOUND_VOLUME = .3;
}
