package javafx.checkboxsnake.data;

/**
 * --- here javadoc ---
 *
 * @author dnolte
 */
public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT;
}
