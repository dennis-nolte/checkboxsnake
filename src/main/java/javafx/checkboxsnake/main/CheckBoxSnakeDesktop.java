package javafx.checkboxsnake.main;

import javafx.checkboxsnake.game.GameFrameController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.io.IOException;

public class CheckBoxSnakeDesktop extends Application {

    private StackPane rootPane = null;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("GameFrame.fxml"));
        rootPane = fxmlLoader.load();

        Scene scene = new Scene(rootPane);

        GameFrameController controller = fxmlLoader.getController();
        controller.setScene(scene);
        controller.initGame();

        Platform.runLater(() -> {
            primaryStage.setHeight(rootPane.getHeight() + 50);
            primaryStage.setWidth(rootPane.getWidth() + 30);
        });

        primaryStage.setScene(scene);
        primaryStage.setTitle("CheckBoxSnake");
        primaryStage.show();
    }
}
