package javafx.checkboxsnake.main;

import javafx.checkboxsnake.game.GameFrameController;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;

import java.io.IOException;

public class BasicView extends View {

    public void init() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("GameFrame.fxml"));

        this.setCenter(fxmlLoader.load());

        GameFrameController controller = fxmlLoader.getController();

        Platform.runLater(() -> {
            controller.setScene(getScene());
            controller.initGame();
        });
    }

    @Override
    protected void updateAppBar(AppBar appBar) {
        appBar.setNavIcon(MaterialDesignIcon.MENU.button(e -> System.out.println("Menu")));
        appBar.setTitleText("Basic View");
        appBar.getActionItems().add(MaterialDesignIcon.SEARCH.button(e -> System.out.println("Search")));
    }
}
