package javafx.checkboxsnake.main;

import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.visual.Swatch;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

public class CheckBoxSnakeMobile extends MobileApplication {

    public CheckBoxSnakeMobile() {
    }

    @Override
    public void init() throws IOException {
        BasicView basicView = new BasicView();
        basicView.init();

        addViewFactory(HOME_VIEW, () -> basicView);
    }

    @Override
    public void postInit(Scene scene) {
        Swatch.BLUE.assignTo(scene);

        ((Stage) scene.getWindow()).getIcons().add(new Image(CheckBoxSnakeMobile.class.getResourceAsStream("/icon.png")));
    }

    public static void main(String[] args) {
        launch(args);
    }
}
