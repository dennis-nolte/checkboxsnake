# CheckBoxSnake

A small javafx based snake game made by simple elements of javafx.\
Type ``./gradlew run`` to run the application. Use arrow keys to play.

## Enviorment
- java 11
- javafx17
- gradle-wrapper 6.4

## Features

- sounds
- transitions
- animated score view
- special food appears every x game loop count
- different pixel implementation like RadioButtonPixel
- started to use gluon, to bring this application on android or as a native windows application
- GameSettings class file to set configurations of the game

## Whats missing

- unit tests
- javadoc
- a special food implementation for RadionButtonPixel
- enhance the special food feature
- complete gluon support for android, windows

## Screenshots
![Screenshot (1)](https://i.ibb.co/7pnfbxy/checkboxsnake1.png)
![Screenshot (2)](https://i.ibb.co/P5x7WwQ/checkboxsnake2.png)
